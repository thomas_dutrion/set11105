﻿using System.Collections.Generic;
using Newtonsoft.Json;
using WebApi.Hal;

namespace BlogEngine.Resources
{
    public class UserRepresentation : Representation
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }

        public override string Rel
        {
            get { return LinkTemplates.Users.User.Rel; }
            set { }
        }

        public override string Href
        {
            get { return LinkTemplates.Users.User.CreateLink(new { id = Id }).Href; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            
        }

       
    }
}
