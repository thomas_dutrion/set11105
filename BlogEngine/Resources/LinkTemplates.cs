﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Hal;

namespace BlogEngine.Resources
{
    public static class LinkTemplates
    {
        public static class Tags
        {
            /// <summary>
            /// /api/tags
            /// </summary>
            public static Link GetTags { get { return new Link("tags", "/api/tags"); } }

            /// <summary>
            /// /api/tags/{id}
            /// </summary>
            public static Link Tag { get { return new Link("tag", "/api/tags/{id}"); } }

        }

        public static class Blogs
        {
            /// <summary>
            /// /api/blogs
            /// </summary>
            public static Link GetBlogs { get { return new Link("blogs", "/api/blogs"); } }

            /// <summary>
            /// /api/blogs/{id}
            /// </summary>
            public static Link Blog { get { return new Link("blogs", "/api/blogs/{id}"); } }
        }

        public static class Users
        {
            /// <summary>
            /// /api/users
            /// </summary>
            public static Link GetUsers { get { return new Link("users", "/api/users"); } }

            /// <summary>
            /// /api/users/{id}
            /// </summary>
            public static Link User { get { return new Link("users", "/api/users/{id}"); } }
        }
    }
}