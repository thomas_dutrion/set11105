﻿using System.Collections.Generic;
using Newtonsoft.Json;
using WebApi.Hal;
using BlogEngine.Models;
using System;

namespace BlogEngine.Resources
{
    public class BlogRepresentation : Representation
    {
        private Blog b;

        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Boolean Approved { get; set; }
        public DateTime? Date { get; set; }

        public BlogRepresentation()
        {
            this.Tags = new List<TagRepresentation>();
            this.Related = new List<BlogRepresentation>();
        }

        [JsonIgnore]
        public virtual UserRepresentation Author { get; set; }
        
        [JsonIgnore]
        public virtual List<TagRepresentation> Tags { get; set; }

        [JsonIgnore]
        public Blog Previous { get; set; }

        [JsonIgnore]
        public Blog Next { get; set; }

        [JsonIgnore]
        public List<BlogRepresentation> Related { get; set; }

        public override string Rel
        {
            get { return LinkTemplates.Blogs.Blog.Rel; }
            set { }
        }

        public override string Href
        {
            get { return LinkTemplates.Blogs.Blog.CreateLink(new { id = Id }).Href; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            
            if (Next != null)
            {
                Links.Add(LinkTemplates.Blogs.Blog.CreateLink("next", new { id = Next.Id }));
            }
            if (Previous != null)
            {
                Links.Add(LinkTemplates.Blogs.Blog.CreateLink("prev", new { id = Previous.Id }));
            }
        }
    }
}
