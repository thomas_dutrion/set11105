﻿using System.Collections.Generic;
using Newtonsoft.Json;
using WebApi.Hal;

namespace BlogEngine.Resources
{
    public class TagRepresentation : Representation
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public bool Approved { get; set; }

        public override string Rel
        {
            get { return LinkTemplates.Tags.Tag.Rel; }
            set { }
        }

        public override string Href
        {
            get { return LinkTemplates.Tags.Tag.CreateLink(new { id = Id }).Href; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            
        }
    }
}
