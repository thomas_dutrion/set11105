﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Hal;

namespace BlogEngine.Resources
{
    public class TagListRepresentation : SimpleListRepresentation<TagRepresentation>
    {
        public TagListRepresentation(IList<TagRepresentation> tags)
            : base(tags)
        {
        }

        protected override void CreateHypermedia()
        {
            Href = LinkTemplates.Tags.GetTags.Href;

            Links.Add(new Link { Href = Href, Rel = "self" });
        }
    }
}