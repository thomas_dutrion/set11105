﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Hal;

namespace BlogEngine.Resources
{
    public class BlogListRepresentation : SimpleListRepresentation<BlogRepresentation>
    {

        public BlogListRepresentation(IList<BlogRepresentation> blogs)
            : base(blogs)
        {
        }

        protected override void CreateHypermedia()
        {
            Href = LinkTemplates.Blogs.GetBlogs.Href;

            Links.Add(new Link { Href = Href, Rel = "self" });
        }
    }
}