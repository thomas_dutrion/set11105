﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using BlogEngine.Models;
using PagedList;

namespace BlogEngine.Controllers
{
    [Authorize]
    public class BlogController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        // GET: AdminBlogs
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                return View(db.Blogs.ToList());
            }
            string userId = User.Identity.GetUserId();
            return View(db.Blogs.Where(b => b.Author.Id == userId).ToList());
        }

        [AllowAnonymous]
        public ActionResult List(int? page)
        {
            return View(this.ListBlogs(page));
        }

        private IPagedList<Blog> ListBlogs(int? page)
        {
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return db.Blogs.Where(b => b.Approved == true).OrderBy(b => b.Id).ToPagedList(pageNumber, pageSize);
        }

        // GET: AdminBlogs/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = null;
            if (User.IsInRole("Admin"))
            {
                blog = db.Blogs.Find(id);
            }
            else
            {
                string userId = User.Identity.GetUserId();
                blog = db.Blogs.Where(b => b.Id == id && b.Author.Id == userId).SingleOrDefault();
            }
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // GET: AdminBlogs/Create
        public ActionResult Create()
        {
            ViewBag.Tags = this.GetTagsList();
            return View();
        }

        // POST: AdminBlogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Content,Approved,Tags,TagsId")] Blog blog)
        {
            List<Tag> allTags = this.GetTagsList();
            if (ModelState.IsValid)
            {
                blog.Author = db.Users.Find(User.Identity.GetUserId());
                var tags = db.Tags.Where(m => blog.TagsId.Contains(m.Id)).ToList();
                blog.Tags = tags;
                if (!User.IsInRole("Admin"))
                {
                    blog.Approved = false;
                }
                if (User.IsInRole("Admin") && blog.Approved)
                {
                    foreach (Tag t in blog.Tags) { t.Approved = true; }
                }
                db.Blogs.Add(blog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Tags = allTags;
            return View(blog);
        }

        // GET: AdminBlogs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = null;
            if (User.IsInRole("Admin"))
            {
                blog = db.Blogs.Find(id);
            }
            else
            {
                string userId = User.Identity.GetUserId();
                blog = db.Blogs.Where(b => b.Id == id && b.Author.Id == userId).Single();
            }
            if (blog == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tags = this.GetTagsList(blog.Tags.ToList());
            return View(blog);
        }

        // POST: AdminBlogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Content,Approved,Tags,TagsId")] Blog blog)
        {
            string userId = User.Identity.GetUserId();
            if (!User.IsInRole("Admin") && blog.Author != null && !blog.Author.Id.Equals(userId))
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                db.Blogs.Attach(blog);
                using (var dbCtx = new BlogDbContext())
                { 
                    Blog existingBlog = dbCtx.Blogs.Find(blog.Id);
                    List<Tag> deletedTags = existingBlog.Tags.Where(t => !blog.TagsId.Contains(t.Id)).ToList<Tag>();
                    List<Tag> addedTags = dbCtx.Tags.Where(m => blog.TagsId.Contains(m.Id)).ToList<Tag>();
                    deletedTags.ForEach(t => existingBlog.Tags.Remove(t));
                    foreach (Tag t in addedTags)
                    {
                        existingBlog.Tags.Add(t);
                        if (User.IsInRole("Admin") && blog.Approved)
                        {
                            t.Approved = true;
                        }
                    }
                    dbCtx.SaveChanges();
                }
                if (!User.IsInRole("Admin"))
                {
                    blog.Approved = false;
                }
                db.Entry(blog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Tags = this.GetTagsList(blog.Tags);
            return View(blog);
        }

        // GET: AdminBlogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = null;
            if (User.IsInRole("Admin"))
            {
                blog = db.Blogs.Find(id);
            }
            else
            {
                string userId = User.Identity.GetUserId();
                blog = db.Blogs.Where(b => b.Id == id && b.Author.Id == userId).Single();
            }
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // POST: AdminBlogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Blog blog = null;
            if (User.IsInRole("Admin"))
            {
                blog = db.Blogs.Find(id);
            }
            else
            {
                blog = db.Blogs.Where(b => b.Id == id && b.Author.Id == User.Identity.GetUserId()).Single();
            }
            if (blog == null)
            {
                return HttpNotFound();
            }
            db.Blogs.Remove(blog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected List<Tag> GetTagsList(List<Tag> tags = null) 
        {
            if (tags != null && tags.Count() > 0)
            {
                List<int> tagsId = tags.Select(t => t.Id).ToList();
                return db.Tags.Where(t => t.Approved == true || tagsId.Contains(t.Id)).OrderBy(t => t.Value).ToList();
            }
            return db.Tags.Where(t => t.Approved == true).OrderBy(t => t.Value).ToList();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
