﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using BlogEngine.Models;
using BlogEngine.Resources;

namespace BlogEngine.Controllers
{
    [System.Web.Http.RoutePrefix("api")]
    public class ApiController : System.Web.Http.ApiController
    {
        private BlogDbContext db = new BlogDbContext();

        // GET: api/blogs
        [System.Web.Mvc.Route("blogs")]
        [System.Web.Mvc.HttpGet]
        public BlogListRepresentation GetBlog()
        {
            List<BlogRepresentation> finalList = new List<BlogRepresentation>();
            foreach (Blog blog in db.Blogs.Where(b => b.Approved == true).OrderByDescending(b => b.Date).ToList<Blog>()) 
            {
                finalList.Add(this.GetBlogRepresentation(blog));
            }
            return new BlogListRepresentation(finalList);
        }

        // GET: api/blogs/5
        [System.Web.Http.Route("blogs/{id}")]
        [System.Web.Http.HttpGet]
        public BlogRepresentation GetBlog(int id)
        {
            Blog blog = db.Blogs.Where(b => b.Id == id && b.Approved == true).SingleOrDefault();
            return this.GetBlogRepresentation(blog);
        }

        // GET: api/blogs/first
        [System.Web.Http.Route("blogs/first")]
        [System.Web.Http.HttpGet]
        public BlogRepresentation GetFirstBlog()
        {
            Blog blog = db.Blogs.Where(b => b.Approved == true).OrderByDescending(b => b.Date).FirstOrDefault();
            return this.GetBlogRepresentation(blog);
        }


        // GET: api/tags
        [System.Web.Http.Route("tags")]
        [System.Web.Http.HttpGet]
        [AcceptVerbs("GET")]
        public TagListRepresentation GetTag()
        {
            List<TagRepresentation> finalList = new List<TagRepresentation>();
            foreach (Tag tag in db.Tags.Where(t => t.Approved == true).ToList<Tag>())
            {
                finalList.Add(new TagRepresentation() { Id = tag.Id, Value = tag.Value, Approved = tag.Approved });
            }
            return new TagListRepresentation(finalList);
        }

        // GET: api/tags/5
        [System.Web.Http.Route("tags/{id}")]
        [System.Web.Http.HttpGet]
        public TagRepresentation GetTag(int id)
        {
            var tag = db.Tags.Find(id);

            return new TagRepresentation
            {
                Id = tag.Id,
                Value = tag.Value
            };
        }

        // POST: api/tags
        [System.Web.Http.Route("tags")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Authorize]
        public TagRepresentation PostTag(Tag tag)
        {
            if (!ModelState.IsValid)
            {
                return new TagRepresentation();
            }
            if (db.Tags.Where(t => t.Value == tag.Value).Count() > 0)
            {
                tag = db.Tags.Where(t => t.Value == tag.Value).FirstOrDefault();
            }
            else
            {
                db.Tags.Add(tag);
                db.SaveChanges();
            }
            return new TagRepresentation() { Id = tag.Id, Value = tag.Value };
        }

        private BlogRepresentation GetBlogRepresentation(Blog blog, Boolean withRelations = true) 
        {
            if (blog != null)
            {
                UserRepresentation authorRepresentation = null;
                List<TagRepresentation> tagsRepresentation = new List<TagRepresentation>();
                Blog nextBlog = null;
                Blog previousBlog = null;
                List<BlogRepresentation> related = new List<BlogRepresentation>();
                if (withRelations)
                {
                    if (blog.Author != null)
                    {
                        authorRepresentation = new UserRepresentation()
                        {
                            Id = blog.Author.Id,
                            Username = blog.Author.UserName,
                            Email = blog.Author.Email
                        };
                    }
                    foreach (Tag tag in blog.Tags.ToList())
                    {
                        TagRepresentation tagRepresentation = new TagRepresentation()
                        {
                            Approved = tag.Approved,
                            Id = tag.Id,
                            Value = tag.Value
                        };
                        tagsRepresentation.Add(tagRepresentation);
                    }
                    nextBlog = db.Blogs.Where(b => b.Approved == true && b.Date > blog.Date).OrderBy(b => b.Date).FirstOrDefault();
                    previousBlog = db.Blogs.Where(b => b.Approved == true && b.Date < blog.Date).OrderByDescending(b => b.Date).FirstOrDefault();
                    foreach (Blog b in db.Blogs.Where(b => b.Approved == true && b.Id != blog.Id && b.Tags.Any(t => blog.TagsId.Contains(t.Id))).Take(3).ToList())
                    {
                        related.Add(this.GetBlogRepresentation(b, false));
                    }
                }
                return new BlogRepresentation()
                {
                    Approved = blog.Approved,
                    Content = blog.Content,
                    Date = blog.Date,
                    Id = blog.Id, 
                    Title = blog.Title,
                    Next = nextBlog,
                    Previous = previousBlog,
                    Author = blog.Author == null ? null : authorRepresentation,
                    Tags = tagsRepresentation,
                    Related = related
                };
            }
            return null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}