﻿$("#tagForm").submit(function (event) {
    event.preventDefault();
    $.ajax({
        url: "/api/tags",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ Value: $('#Value').val() }),
        success: function (response) {
            if (response.Value != null && ($("#TagsId option[value='" + response.Id + "']").length < 1)) {
                $('#TagsId').append('<option value="' + response.Id + '" selected="selected">' + response.Value + '</option>');
                $('#TagsId').trigger('chosen:updated');
            }
            $('#addTag').modal('hide');
        },
        error: function () {
            alert("There was an error... please try again.");
        }
    });
    return false;
});

$('#addTag').on('hidden.bs.modal', function (e) {
    $('#Value').val("");
});