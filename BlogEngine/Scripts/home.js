﻿$('#next').attr('disabled', 'disabled');
$('#prev').attr('disabled', 'disabled');

fetchAndPopulateBlog('/api/blogs/first');

$('body').on('click', '.ajaxContent', function (event) {
    event.preventDefault();
    fetchAndPopulateBlog($(this).attr('href'));
});

function fetchAndPopulateBlog(url) {
    $.ajax({
        headers: {
            Accept: "application/hal+json; charset=utf-8"
        },
        url: url,
        success: function (blog) {
            populateBlog(blog);
        }
    });
}

function populateBlog(blog) {
    $('#loader').hide();
    if (blog != null && typeof blog != 'undefined') {
        $('#content').show();
        $('#emptyAlert').hide();
        $("#content h1").text(blog.Title);
        $("#content div.row article").html(blog.Content);
        $("#content aside.info #date").text(new Date(blog.Date).toLocaleString());

        if (typeof blog._links.prev != 'undefined') {
            $("#prev").attr('href', blog._links.prev.href).removeAttr('disabled');
        } else {
            $("#prev").attr('href', '#').attr('disabled', 'disabled');
        }
        if (typeof blog._links.next != 'undefined') {
            $("#next").attr('href', blog._links.next.href).removeAttr('disabled');
        } else {
            $("#next").attr('href', '#').attr('disabled', 'disabled');
        }

        $('#content aside.info #tags').text('');
        if (Object.prototype.toString.call(blog._embedded.tag) === '[object Object]') {
            $('#content aside.info #tags').append('<span class="label label-default">' + blog._embedded.tag.Value + '</span> ');
        }
        if (Object.prototype.toString.call(blog._embedded.tag) === '[object Array]') {
            blog._embedded.tag.forEach(function (t) {
                $('#content aside.info #tags').append('<span class="label label-default">' + t.Value + '</span> ');
            });
        }


        if (Object.prototype.toString.call(blog._embedded.blogs) === '[object Object]') {
            $("#content div.row aside").html('<h2>Related articles</h2><div class="list-group">');
            $('#content div.row aside').append('<a href="' + blog._embedded.blogs._links.self.href + '" class="list-group-item ajaxContent">' + blog._embedded.blogs.Title + '</a>');
        } else if (Object.prototype.toString.call(blog._embedded.blogs) === '[object Array]') {
            $("#content div.row aside").html('<h2>Related articles</h2><div class="list-group">');
            blog._embedded.blogs.forEach(function (t) {
                $('#content div.row aside').append('<a href="' + t._links.self.href + '" class="list-group-item ajaxContent">' + t.Title + '</a> ');
            });
        } else {
            $("#content div.row aside").html('');
        }

        $("#content div.row aside").append('</div>');

        if (Object.prototype.toString.call(blog._embedded.users) === '[object Object]') {
            $('#content aside.info #author').text(blog._embedded.users.Username);
        }
    } else {
        $('#content').hide();
        $('#emptyAlert').show();
    }
}