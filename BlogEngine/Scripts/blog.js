﻿$(document).ready(
    function () {
        $('textarea').redactor();
        $('select[multiple="multiple"]').chosen();
    }
);
