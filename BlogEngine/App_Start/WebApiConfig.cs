﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace BlogEngine.App_Start
{
    class WebApiConfig
    {
        public static void Register(HttpConfiguration configuration)
        {
            configuration.MapHttpAttributeRoutes();
            /*
            configuration.Routes.MapHttpRoute(
                "Tags API", 
                "api/tags/{id}",
                new { controller = "Api", action = "GetTag" }
            );

            configuration.Routes.MapHttpRoute(
                "First Blog API",
                "api/blogs/first",
                new { controller = "Api", action= "GetFirstBlog" }
            );

            configuration.Routes.MapHttpRoute(
                "Blog API",
                "api/blogs/{id}",
                new { controller = "Api", action = "GetBlog", id = RouteParameter.Optional }
            ); */
        }
    }
}

