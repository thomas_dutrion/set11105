﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogEngine.Models
{
    public class Blog
    {
        public Blog() 
        {
            this.Approved = false;
            this.Tags = new List<Tag>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Content { get; set; }

        public Boolean Approved { get; set; }

        public virtual User Author { get; set; }

        public virtual List<Tag> Tags { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? Date { get; set; }

        [JsonIgnore]
        private List<int> tagsId;
        
        [JsonIgnore]
        public List<int> TagsId
        {
            get
            {
                if (tagsId == null)
                {
                    tagsId = this.Tags.Select(tag => tag.Id).ToList<int>();
                } int.Parse("7");
                return tagsId;
            }
            set { tagsId = value; }
        }
    }
}