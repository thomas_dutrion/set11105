﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlogEngine.Models
{
    public class Tag
    {

        public Tag()
        {
            this.Approved = false;
        }

        public Tag(int tagId)
            : base()
        {
            this.Id = tagId;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Value { get; set; }

        public bool Approved { get; set; }

        [JsonIgnore]
        public virtual List<Blog> Blogs { get; set; }
    }
}