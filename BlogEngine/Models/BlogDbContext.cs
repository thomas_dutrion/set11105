﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace BlogEngine.Models
{
    
    public class BlogDbContext : IdentityDbContext<User>
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Tag> Tags { get; set; }

        // Add throwIfV1Schema: false to make it work on Windows Azure
        public BlogDbContext() : base("DefaultConnection", throwIfV1Schema: false)
        {
            Configuration.LazyLoadingEnabled = true;
        }
        public static BlogDbContext Create()
        {
            return new BlogDbContext();
        }
    }
}